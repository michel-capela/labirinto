const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

let mapa = document.getElementById("mapa");
for (line of map) {
    let linha = document.createElement("div");
    linha.className = "linha";

    for (cell of line) {
        let celula = document.createElement("div");
        linha.appendChild(celula);
        celula.className = "celula";
        celula.innerText = cell;

    }
    mapa.appendChild(linha);
}

'use strict';
let victoryMsg = document.createElement("div");
victoryMsg.innerText = "Parabens, voce venceu!"

let boxTop = 360;
let boxLeft = 0;
let retorno;
let boxTopValidation = boxTop;
let boxLeftValidation = boxLeft;

document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    switch (keyName) {
        case "ArrowRight":
            boxLeftValidation += 40;
            retorno = valida(document.elementFromPoint(boxLeftValidation, boxTopValidation).textContent);
            retorno == false ? boxLeft += 40 : boxLeftValidation = boxLeft;
            break;
        case "ArrowLeft":
            boxLeftValidation -= 40;
            retorno = valida(document.elementFromPoint(boxLeftValidation, boxTopValidation).textContent);
            retorno == false ? boxLeft -= 40 : boxLeftValidation = boxLeft;

            break;
        case "ArrowUp":
            boxTopValidation -= 40;
            retorno = valida(document.elementFromPoint(boxLeftValidation, boxTopValidation).textContent);
            retorno == false ? boxTop -= 40 : boxTopValidation = boxTop;
            break;
        case "ArrowDown":
            boxTopValidation += 40;
            retorno = valida(document.elementFromPoint(boxLeftValidation, boxTopValidation).textContent);
            retorno == false ? boxTop += 40 : boxTopValidation = boxTop;
            break;
    }
    document.getElementById("box").style.top = boxTop + "px";
    document.getElementById("box").style.left = boxLeft + "px";
})

function valida(parametro) {

    if (parametro === "F") {
        boxLeft = 0;
        boxTop = 360;
        document.body.appendChild(victoryMsg);

    } else if (parametro === "W" || parametro === "S") {
        return true;
    } else {
        return false;
    }
}